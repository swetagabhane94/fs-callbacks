const fs = require("fs");

function problem2(folderName) {
  fs.readFile(folderName, "utf-8", (err, fileContents) => {
    if (err) {
      console.error(`Error occured on lipsum file: ${err}`);
    } else {
      console.log(`Lipsum file read done`);

      let upperCaseConversion = fileContents.toUpperCase();
      let newFile1 = "upperCase.txt";

      fs.writeFile(`./${newFile1}`, upperCaseConversion, "utf-8", (err) => {
        if (err) {
          console.error(`Error occured on upperCase file: ${err}`);
        } else {
          console.log(`Uppercase conversion done from lipsum file`);

          fs.readFile(newFile1, "utf-8", (err, upperCaseData) => {
            let lowerCaseConversion = upperCaseData.toLowerCase().split(". ");
            let newFile2 = "lowerCase.txt";

            fs.writeFile(`${newFile2}`, lowerCaseConversion, "utf-8", (err) => {
              if (err) {
                console.error(`Error occured on lowerCase file: ${err}`);
              } else {
                console.log(`LowerCase conversion and sort the file`);

                fs.readFile(newFile2, "utf-8", (err, lowerCaseData) => {
                  let sorting = lowerCaseData.split(" ").sort();
                  let newFile3 = "sorting.txt";

                  fs.writeFile(`${newFile3}`, sorting, "utf-8", (err) => {
                    if (err) {
                      console.error(`Error occured on sorting file: ${err}`);
                    } else {
                      console.log(`Sorting the file`);
                    }
                  });

                  fs.appendFile("../fileNames.txt", newFile3 + " ", "utf-8", (err) => {
                      if (err) {
                        console.error(`Error occured on append file 3: ${err}`);
                      } else {
                        console.log(`Adding file3 in main file`);

                        fs.readFile("../fileNames.txt", (err) => {
                          if (err) {
                            console.error(`Error occured on read file: ${err}`);
                          } else {
                            console.log(`Reading file done`);
                          }
                        });

                        fs.unlink(`${newFile1}`, (err) => {
                          if (err) {
                            console.error(`Error occured on delete file 1: ${err}`);
                          } else {
                            console.log(`Deleted the newFile1 file`);
                          }
                        });

                        fs.unlink(`${newFile2}`, (err) => {
                          if (err) {
                            console.error(`Error occured on delete file 2: ${err}`);
                          } else {
                            console.log(`Deleted the newFile2 file`);
                          }
                        });

                        fs.unlink(`${newFile3}`, (err) => {
                          if (err) {
                            console.error(`Error occured on delete file 3: ${err}`);
                          } else {
                            console.log(`Deleted the newFile3 file`);
                          }
                        });
                      }
                    }
                  );
                });
              }
            });
            fs.appendFile("../fileNames.txt", newFile2 + " ", "utf-8", (err) => {
                if (err) {
                  console.error(`Error occured on append file 2: ${err}`);
                } else {
                  console.log(`Adding file2 in main file`);
                }
              }
            );
          });
        }
      });
      fs.writeFile("../fileNames.txt", newFile1 + " ", "utf-8", (err) => {
        if (err) {
          console.error(`Error occured on write file 1: ${err}`);
        } else {
          console.log(`Moving file1 in main file`);
        }
      });
    }
  });
}

module.exports = problem2;
