const fs = require("fs");

function problem1(number, folderName) {
  fs.mkdir(folderName, (err) => {
    if (err) {
      console.error(`Error occured on directory: ${err}`);
    } else {
      console.log(`Create the node directory`);

      for (let index = 1; index <= number; index++) {
        let fileCreate = `newfile` + index + `.json`;

        fs.writeFile(`${folderName}/${fileCreate}`, JSON.stringify({ a: 1, b: 2, c: 3 }),(err) => {
            if (err) {
              console.error(`Error occured on write file: ${err}`);
            } else {
              console.log(`Create the file: ${fileCreate}`);

              fs.unlink(`${folderName}/${fileCreate}`, (err) => {
                if (err) {
                  console.error(`Error occured on delete file: ${err}`);
                } else {
                  console.log(`Deleted the file: ${fileCreate}`);
                }
              });
            }
          }
        );
      }
    }
  });
}

module.exports = problem1;
