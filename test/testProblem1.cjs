const path = require("path");
const problem1 = require("../problem1.cjs");

const number = Math.round(Math.random() * 10);
const folderName = path.resolve("../node");

problem1(number, folderName);
